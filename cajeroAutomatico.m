#import <Foundation/Foundation.h>
#import <conio.h>
#import <stdlib.h>
#import <time.h>
#import "cajeroautomatico.h"
#import "usuario.h"
#import "usuariomaestro.h"
#import "panel.h"
#import "slot.h"

@implementation cajeroAutomaticoITF

-(void)iniciarBaseDatosCajero:(CajeroAutomatico*)cajeroAutomatico {
	Usuario *nuevoUsuario;
	UsuarioMaestro *nuevoUsuarioMaestro;
	cajeroAutomatico->listaUsuarios = [[NSMutableArray alloc] init];
	cajeroAutomatico->listaPaneles = [[NSMutableArray alloc] init];
	usuarioITF *interfazUsuario = [[usuarioITF alloc] init];
	usuarioMaestroITF *interfazUsuarioMaestro = [[usuarioMaestroITF alloc] init];
	cajeroAutomatico->usuariosRegistrados = 0;
	
	//Se empieza a crear un registro de 10 usuarios 
	nuevoUsuario = [interfazUsuario nuevoUsuario: @"Andres" conCuenta: @"093343263" conClave: 2395 ySaldo: 757];
	[cajeroAutomatico->listaUsuarios addObject: [NSValue valueWithPointer: nuevoUsuario]];
	nuevoUsuario = [interfazUsuario nuevoUsuario: @"Clara" conCuenta: @"095674318" conClave: 6743 ySaldo: 1020];
	[cajeroAutomatico->listaUsuarios addObject: [NSValue valueWithPointer: nuevoUsuario]];
	nuevoUsuario = [interfazUsuario nuevoUsuario: @"Wilson" conCuenta: @"096653287" conClave: 9610 ySaldo: 536];
	[cajeroAutomatico->listaUsuarios addObject: [NSValue valueWithPointer: nuevoUsuario]];
	nuevoUsuario = [interfazUsuario nuevoUsuario: @"Diana" conCuenta: @"095677432" conClave: 1234 ySaldo: 678];
	[cajeroAutomatico->listaUsuarios addObject: [NSValue valueWithPointer: nuevoUsuario]];
	nuevoUsuario = [interfazUsuario nuevoUsuario: @"Oscar" conCuenta: @"092345687" conClave: 8765 ySaldo: 234];
	[cajeroAutomatico->listaUsuarios addObject: [NSValue valueWithPointer: nuevoUsuario]];
	nuevoUsuario = [interfazUsuario nuevoUsuario: @"Ronald" conCuenta: @"091230987" conClave: 4529 ySaldo: 888];
	[cajeroAutomatico->listaUsuarios addObject: [NSValue valueWithPointer: nuevoUsuario]];
	nuevoUsuario = [interfazUsuario nuevoUsuario: @"Kleber" conCuenta: @"096582133" conClave: 1092 ySaldo: 222];
	[cajeroAutomatico->listaUsuarios addObject: [NSValue valueWithPointer: nuevoUsuario]];
	nuevoUsuario = [interfazUsuario nuevoUsuario: @"Laura" conCuenta: @"098717685" conClave: 6548 ySaldo: 634];
	[cajeroAutomatico->listaUsuarios addObject: [NSValue valueWithPointer: nuevoUsuario]];
	nuevoUsuario = [interfazUsuario nuevoUsuario: @"Sofia" conCuenta: @"097223221" conClave: 1056 ySaldo: 809];
	[cajeroAutomatico->listaUsuarios addObject: [NSValue valueWithPointer: nuevoUsuario]];
	nuevoUsuario = [interfazUsuario nuevoUsuario: @"Javier" conCuenta: @"094598925" conClave: 4761 ySaldo: 1044];
	[cajeroAutomatico->listaUsuarios addObject: [NSValue valueWithPointer: nuevoUsuario]];
	cajeroAutomatico->usuariosRegistrados = 10;
	nuevoUsuarioMaestro = [interfazUsuarioMaestro iniciarUsuarioMaestro: @"Wellington" conCuenta: @"0923071583" conClave: 9863 supervisara: cajeroAutomatico->listaUsuarios];
	cajeroAutomatico->usuarioMaestro = nuevoUsuarioMaestro;
}

-(void)menuAcceso:(CajeroAutomatico*)cajeroAutomatico {
	NSString *nombre;
	char entrada[100];
	cajeroAutomaticoITF *interfazCajeroAutomatico = [[cajeroAutomaticoITF alloc] init];
	NSLog(@"\t\tBIENVENIDO");
	NSLog(@"Ingrese su tarjeta de credito (nombre de usuario)");
	scanf("%s", entrada);
	nombre = [NSString stringWithCString: entrada encoding: NSASCIIStringEncoding];
	[interfazCajeroAutomatico buscarUsuario: nombre dadaLaInterfaz: cajeroAutomatico];
}

-(void)buscarUsuario:(NSString*)nombre dadaLaInterfaz:(CajeroAutomatico*)cajeroAutomatico {
	int i;
	Usuario *usuariobuscado;
	cajeroAutomaticoITF *interfazCajeroAutomatico = [[cajeroAutomaticoITF alloc] init];
	for(i = 0; i < cajeroAutomatico->usuariosRegistrados; i++) {
		usuariobuscado = [[cajeroAutomatico->listaUsuarios objectAtIndex: i] pointerValue];
		if([nombre isEqualToString: usuariobuscado->nombre] || [nombre isEqualToString: cajeroAutomatico->usuarioMaestro->nombre]) {
			[interfazCajeroAutomatico menuIngreso: usuariobuscado dadaLaInterfaz: cajeroAutomatico];
		}else if(i == cajeroAutomatico->usuariosRegistrados - 1)
			NSLog(@"Usted no se encuentra registrado en el sistema");
	}
	return;
}

-(void)menuIngreso:(Usuario*)usuario dadaLaInterfaz:(CajeroAutomatico*)cajeroAutomatico {
	int i, secuenciaIngresada, posicion1, posicion2;
	Panel *nuevoPanel;
	Slot *caracterCodificado = malloc(sizeof(Slot));
	NSMutableArray *claveReconstruida;
	slotITF *interfazSlot = [[slotITF alloc] init];
	usuarioITF *interfazUsuario = [[usuarioITF alloc] init];
	usuarioMaestroITF *interfazUsuarioMaestro = [[usuarioMaestroITF alloc] init];
	cajeroAutomaticoITF *interfazCajeroAutomatico = [[cajeroAutomaticoITF alloc] init];
	panelITF *interfazPanel = [[panelITF alloc] init];
	
	//creamos la interfaz que se mostrara al usuario
	for(i = 0; i < TAMANIOCLAVE; i++) {
		caracterCodificado->caracter = [[usuario->claveCodificada objectAtIndex: i] pointerValue];
		caracterCodificado->esClaveCodificada = 1;
		nuevoPanel = [interfazPanel crearPanelConClave: caracterCodificado conPosicion: i + 1];
		[cajeroAutomatico->listaPaneles addObject: [NSValue valueWithPointer: nuevoPanel]];
	}
	for(i = TAMANIOCLAVE; i < NUMEROPANELES; i++) {
		caracterCodificado = [interfazSlot crearCaracterAleatorio: 0];
		nuevoPanel = [interfazPanel crearPanelConClave: caracterCodificado conPosicion: i + 1];
		[cajeroAutomatico->listaPaneles addObject: [NSValue valueWithPointer: nuevoPanel]];
	}
	[interfazCajeroAutomatico desordenarPaneles: cajeroAutomatico->listaPaneles];
	
	//Se muestra la interfaz al usuario
	i = 0;
	posicion1 = 0;
	posicion2 = 1; 
	do{
		[interfazPanel presentarPaneles: [[cajeroAutomatico->listaPaneles objectAtIndex: posicion1] pointerValue] 
		siguientePanel: [[cajeroAutomatico->listaPaneles objectAtIndex: posicion2] pointerValue]];
		posicion1 = posicion1 + 2;
		posicion2 = posicion2 + 2;
		i = i + 2;
	}while(i < NUMEROPANELES);
	
	do{
		NSLog(@"Seleccione una opcion");
		NSLog(@"[1] Ingresar clave");
		NSLog(@"[2] Salir");
		scanf("%d", &i);
		if(i == 1) {
			NSLog(@"Ingrese la secuencia numerica de los numeros que representan su clave:");
			scanf("%d", &secuenciaIngresada);
			claveReconstruida = [interfazCajeroAutomatico reconstruirClave: cajeroAutomatico ingreso: secuenciaIngresada];
			if(claveReconstruida == nil)
				NSLog(@"Secuencia ingresada incorrecta");
			if([interfazUsuarioMaestro verificarClaveMaestra: cajeroAutomatico->usuarioMaestro siTieneClave: claveReconstruida] == 1)
				[interfazUsuarioMaestro menuUsuarioMaestro: cajeroAutomatico->usuarioMaestro];
			else
				[interfazUsuario menuUsuario: usuario];
		}else {
			NSLog(@"Gracias por usar nuestros servicios");
			return;
		}
	}while(i == 1);
}

-(void)desordenarPaneles:(NSMutableArray*)listaPaneles {
	int i, valorAleatorio;
	Panel *panelTemp;
	for(i = 0; i < NUMEROPANELES; i++) {
		valorAleatorio = rand()%5+1;
		panelTemp = [[listaPaneles objectAtIndex: valorAleatorio] pointerValue];
		[listaPaneles replaceObjectAtIndex: valorAleatorio withObject: [NSValue valueWithPointer:[[listaPaneles objectAtIndex: i] pointerValue]]];
		[listaPaneles replaceObjectAtIndex: i withObject: [NSValue valueWithPointer: panelTemp]];
	}
	for(i = 1; i <= NUMEROPANELES; i++) {
		panelTemp = [[listaPaneles objectAtIndex: i - 1] pointerValue];
		panelTemp->posicion = i;
	}
}

-(NSMutableArray*)reconstruirClave:(CajeroAutomatico*)cajeroAutomatico ingreso:(int)secuenciaIngresada {
	NSMutableArray *claveReconstruida = [[NSMutableArray alloc] init];
	
	return claveReconstruida;
}

@end