#import <FOUNDATION/FOUNDATION.H>
#import <conio.h>
#import <stdlib.h>
#import <time.h>
#import "slot.h"

@implementation slotITF

-(NSString*)retornarCaracterCodificado:(Slot*)slot {
	if(slot->esClaveCodificada == 1)
		return slot->caracter;
	else
		return NULL;
}

-(Slot*)crearCaracterAleatorio:(int)esClaveCodificada {
	int i = rand()%2 + 1;
	Slot *nuevoSlot = malloc(sizeof(Slot));
	nuevoSlot->caracter = [NSString stringWithFormat: @"%c", rand()%15 + 65];
	nuevoSlot->esClaveCodificada = esClaveCodificada;
	if(i == 1)
		nuevoSlot->caracter = [NSString stringWithFormat:@"%@%@", nuevoSlot->caracter, @"b"];
	return nuevoSlot;
}

@end
