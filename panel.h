#import <FOUNDATION/FOUNDATION.H>
#import "slot.h"

#define DIMENSIONPANEL 3

typedef struct Panel {
	int posicion;
	NSMutableArray *slots;
}Panel;

@interface panelITF:NSObject

-(Panel*)crearPanel:(Slot*)caracter1 siguienteCaracter:(Slot*)caracter2 siguienteCaracter:(Slot*)caracter3 yPosicion:(int)posicion;
-(Panel*)crearPanelConClave:(Slot*)caracterCodificado conPosicion:(int)posicion;
-(void)presentarPaneles:(Panel*)panel1 siguientePanel:(Panel*)panel2;

@end