#import <Foundation/Foundation.h>
#import <conio.h>
#import <stdlib.h>
#import <time.h>
#import "usuariomaestro.h"
#import "usuario.h"

@implementation usuarioMaestroITF

-(UsuarioMaestro*)iniciarUsuarioMaestro:(NSString*)nombre conCuenta:(NSString*)numeroCuenta conClave:(int)clave supervisara:(NSMutableArray*)listaUsuarios {
	UsuarioMaestro *nuevoUsuarioMaestro = malloc(sizeof(UsuarioMaestro));
	usuarioMaestroITF *interfazUsuarioMaestro = [[usuarioMaestroITF alloc] init];
	nuevoUsuarioMaestro->nombre = nombre;
	nuevoUsuarioMaestro->numeroCuenta = numeroCuenta;
	nuevoUsuarioMaestro->clave = clave;
	nuevoUsuarioMaestro->claveCodificada = [interfazUsuarioMaestro crearClaveCodificada: clave];
	nuevoUsuarioMaestro->listaUsuarios = listaUsuarios;
	return nuevoUsuarioMaestro;
}

-(int)verificarClaveMaestra:(UsuarioMaestro*)usuarioMaestro siTieneClave:(NSMutableArray*)claveIngresada {
	int i;
	for(i = 0; i < TAMANIOCLAVE; i++) {
		if([(NSString*)[[usuarioMaestro->claveCodificada objectAtIndex: i] pointerValue] isEqualToString: (NSString*)[[claveIngresada objectAtIndex: i] pointerValue]] == 0)
			return 0;
	}
	return 1;
}

-(void)menuUsuarioMaestro:(UsuarioMaestro*)usuarioMaestro {
	int op;
	usuarioMaestroITF *interfazUsuarioMaestro = [[usuarioMaestroITF alloc] init];
	do {
	NSLog(@"\t\tMENU PRINCIPAL");
	NSLog(@" [1] Crear usuario\t\tModificar usuario [2]");
	NSLog(@" [3] Eliminar usuario\t\tSalir [4]");
	scanf("%d", &op);
		switch(op){
			case 1:
				[interfazUsuarioMaestro crearUsuario: usuarioMaestro];
			break;
			case 2:
				[interfazUsuarioMaestro modificarUsuario: usuarioMaestro];
			break;
			case 3:
				[interfazUsuarioMaestro eliminarUsuario: usuarioMaestro];
			break;
			default:
			break;
		}
	}while(op<4);
}

-(void)crearUsuario:(UsuarioMaestro*)usuarioMaestro {
	int clave, saldo;
	char nombreTmp[100], numeroCuentaTmp[100];
	NSString *nombre, *numeroCuenta;
	Usuario *nuevoUsuario = malloc(sizeof(Usuario));
	usuarioITF *interfazUsuario = [[usuarioITF alloc] init];
	NSLog(@"Ingrese el nombre del usuario:");
	scanf("%s", nombreTmp);
	NSLog(@"Ingrese el numero de cuenta del usuario:");
	scanf("%s", numeroCuentaTmp);
	NSLog(@"Ingrese la clave del usuario:");
	scanf("%d", &clave);
	NSLog(@"Ingrese el saldo del usuario:");
	scanf("%d", &saldo);
	nombre = [NSString stringWithCString: nombreTmp encoding: NSASCIIStringEncoding];
	numeroCuenta = [NSString stringWithCString: numeroCuentaTmp encoding: NSASCIIStringEncoding];
	nuevoUsuario = [interfazUsuario nuevoUsuario: nombre conCuenta: numeroCuenta conClave: clave ySaldo: saldo];
	[usuarioMaestro->listaUsuarios addObject:[NSValue valueWithPointer: nuevoUsuario]];
	(usuarioMaestro->usuariosRegistrados)++;
}

-(void)modificarUsuario:(UsuarioMaestro*)usuarioMaestro {
	int i, op;
	char entrada[100];
	NSString *temp;
	Usuario *nuevoUsuario = malloc(sizeof(Usuario));
	usuarioMaestroITF *interfazUsuarioMaestro = [[usuarioMaestroITF alloc] init];
	for(i = 0; i < usuarioMaestro->usuariosRegistrados; i++) {
		NSLog(@" [%@] ", i + 1);
		[interfazUsuarioMaestro presentarUsuario: [[usuarioMaestro->listaUsuarios objectAtIndex: i] pointerValue]];
	}
	NSLog(@"Seleccione el usuario que desea modificar:");
	scanf("%d", &i);
	nuevoUsuario = [[usuarioMaestro->listaUsuarios objectAtIndex: i] pointerValue];
	NSLog(@"Seleccione el parametro que desea modificar:");
	NSLog(@" [1] Nombre\tNumero de cuenta [2]");
	NSLog(@" [3] clave\tSaldo [4]");
	NSLog(@" [5] Salir");
	scanf("%d", &op);
	if(i == 1) {
		NSLog(@"Ingrese el nuevo nombre:");
		scanf("%s", entrada);
		temp = [NSString stringWithCString: entrada encoding: NSASCIIStringEncoding];
		nuevoUsuario->nombre = temp;
	}else if(i == 2) {
		NSLog(@"Ingrese el nuevo numero de cuenta:");
		scanf("%s", entrada);
		temp = [NSString stringWithCString: entrada encoding: NSASCIIStringEncoding];
		nuevoUsuario->numeroCuenta = temp;
	}else if(i == 3) {
	NSLog(@"Ingrese la nueva clave (debe tener 4 digitos):");
		scanf("%d", &op);
		nuevoUsuario->clave = op;
		nuevoUsuario->claveCodificada = [interfazUsuarioMaestro crearClaveCodificada: op];
	}else if(i == 4){
		NSLog(@"Ingrese el saldo:");
		scanf("%d", &op);
		nuevoUsuario->saldo = op;
	}else
		return;
	[usuarioMaestro->listaUsuarios addObject:[NSValue valueWithPointer: nuevoUsuario]];
}

-(void)eliminarUsuario:(UsuarioMaestro*)usuarioMaestro {
	int i;
	usuarioMaestroITF *interfazUsuarioMaestro = [[usuarioMaestroITF alloc] init];
	for(i = 0; i < usuarioMaestro->usuariosRegistrados; i++) {
		NSLog(@"\n [%i] ", i + 1);
		[interfazUsuarioMaestro presentarUsuario: [[usuarioMaestro->listaUsuarios objectAtIndex: i] pointerValue]];
	}
	NSLog(@"Seleccione el usuario que desea eliminar:");
	scanf("%d", &i);
	[usuarioMaestro->listaUsuarios removeObjectAtIndex: i];
	(usuarioMaestro->usuariosRegistrados)--;
}

@end