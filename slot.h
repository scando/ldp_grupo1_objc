#import <FOUNDATION/FOUNDATION.H>

typedef struct Slot {
	int posicionEnBloque;
	int esClaveCodificada;
	NSString *caracter;
}Slot;

@interface slotITF:NSObject

-(NSString*)retornarCaracterCodificado:(Slot*)slot;
-(Slot*)crearCaracterAleatorio:(int)esClaveCodificada;

@end