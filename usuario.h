#import <FOUNDATION/FOUNDATION.H>

#define TAMANIOCLAVE 4

typedef struct Usuario {
	int clave;
	int saldo;
	NSString *nombre;
	NSString *numeroCuenta;
	NSMutableArray *claveCodificada;
}Usuario;

@interface usuarioITF:NSObject

-(Usuario*)nuevoUsuario:(NSString*)nombre conCuenta:(NSString*)numeroCuenta conClave:(int)clave ySaldo:(int)saldo;
-(NSMutableArray*)crearClaveCodificada:(int)clave;
-(void)presentarUsuario:(Usuario*)usuario;
-(void)menuUsuario:(Usuario*)usuario;
-(void)depositarSaldo:(Usuario*)usuario depositara:(int)deposito;
-(void)retirarSaldo:(Usuario*)usuario retirara:(int)retiro;
-(void)mostrarClaveCodificada:(Usuario*)usuario;

@end