#import <Foundation/Foundation.h>
#import "usuario.h"
#import "usuariomaestro.h"
#import "panel.h"

#define NUMEROPANELES 6
#define CARACTERESPORPANEL 3
#define MAXIMOUSUARIOS 100

typedef struct CajeroAutomatico {
	int usuariosRegistrados;
	UsuarioMaestro *usuarioMaestro;
	NSMutableArray *listaPaneles;
	NSMutableArray *listaUsuarios;
	NSMutableArray *listaCaracteres;
}CajeroAutomatico;

@interface cajeroAutomaticoITF:NSObject

-(void)iniciarBaseDatosCajero:(CajeroAutomatico*)cajeroAutomatico;
-(void)menuAcceso:(CajeroAutomatico*)cajeroAutomatico;
-(void)buscarUsuario:(NSString*)nombre dadaLaInterfaz:(CajeroAutomatico*)cajeroAutomatico;
-(void)menuIngreso:(Usuario*)usuario dadaLaInterfaz:(CajeroAutomatico*)cajeroAutomatico;
-(void)desordenarPaneles:(NSMutableArray*)listaPaneles;
-(NSMutableArray*)reconstruirClave:(CajeroAutomatico*)cajeroAutomatico ingreso:(int)secuenciaIngresada;

@end

//trisquel <- gnu.org
//Descargar gorm para windows (es un IDE)
//para compilar con interfaz graficausamos GNUmakefile y escribimos make
//la primera vez surgira un error, para corregirlo vamos a GNUstep/...../enviroment y aqui creamo un archivo GNUSTEP_MAKEFILES = "
//guia objective c pag 80: funciones
//guia objective c pag 88: NSNumber
//guia objective c pag 119: NSString








