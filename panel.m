#import <FOUNDATION/FOUNDATION.H>
#import <conio.h>
#import <stdlib.h>
#import <time.h>
#import "panel.h"
#import "slot.h"

@implementation panelITF

-(Panel*)crearPanel:(Slot*)caracter1 siguienteCaracter:(Slot*)caracter2 siguienteCaracter:(Slot*)caracter3 yPosicion:(int)posicion {
	Panel *nuevoPanel = malloc(sizeof(Panel));
	nuevoPanel->slots = [[NSMutableArray alloc] init];
	[nuevoPanel->slots addObject:[NSValue valueWithPointer: caracter1]];
	[nuevoPanel->slots addObject:[NSValue valueWithPointer: caracter2]];
	[nuevoPanel->slots addObject:[NSValue valueWithPointer: caracter3]];
	nuevoPanel->posicion = posicion;
	return nuevoPanel;
}

-(Panel*)crearPanelConClave:(Slot*)caracterCodificado conPosicion:(int)posicion {
	int i = rand()%2 + 1;
	Slot *nuevoSlot1 = malloc(sizeof(Slot)); 
	Slot *nuevoSlot2 = malloc(sizeof(Slot));
	Panel *nuevoPanel;
	panelITF *interfazPanel = [[panelITF alloc] init];
	do{
		nuevoSlot1->caracter = [NSString stringWithFormat: @"%c", rand()%15 + 65];
	}while([nuevoSlot1->caracter isEqualToString: caracterCodificado->caracter]);
	do{
		nuevoSlot2->caracter = [NSString stringWithFormat: @"%c", rand()%15 + 65];
	}while([nuevoSlot1->caracter isEqualToString: nuevoSlot2->caracter] || [nuevoSlot2->caracter isEqualToString: caracterCodificado->caracter]);
	nuevoSlot1->esClaveCodificada = 0;
	nuevoSlot2->esClaveCodificada = 0;
	if(i == 1) {
		nuevoPanel = [interfazPanel crearPanel: caracterCodificado siguienteCaracter: nuevoSlot1 siguienteCaracter: nuevoSlot2 yPosicion: posicion];
	}else if(i == 2) {
		nuevoPanel = [interfazPanel crearPanel: nuevoSlot1 siguienteCaracter: caracterCodificado siguienteCaracter: nuevoSlot2 yPosicion: posicion];
	}else {
		nuevoPanel = [interfazPanel crearPanel: nuevoSlot1 siguienteCaracter: nuevoSlot2 siguienteCaracter: caracterCodificado yPosicion: posicion];
	}
	return nuevoPanel;
}

-(void)presentarPaneles:(Panel*)panel1 siguientePanel:(Panel*)panel2 {
	NSLog(@"[%d] %@   %@   %@   %@   %@   %@ [%d]",panel1->posicion, ((Slot*)[[panel1->slots objectAtIndex: 0] pointerValue])->caracter, ((Slot*)[[panel1->slots objectAtIndex: 1] pointerValue])->caracter, ((Slot*)[[panel1->slots objectAtIndex: 2] pointerValue])->caracter
	, ((Slot*)[[panel2->slots objectAtIndex: 0] pointerValue])->caracter, ((Slot*)[[panel2->slots objectAtIndex: 1] pointerValue])->caracter, ((Slot*)[[panel2->slots objectAtIndex: 2] pointerValue])->caracter, panel2->posicion);
}

@end