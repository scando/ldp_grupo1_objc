#import <FOUNDATION/FOUNDATION.H>
#import <conio.h>
#import <stdlib.h>
#import <time.h>
#import "usuario.h"

@implementation usuarioITF

-(Usuario*)nuevoUsuario:(NSString*)nombre conCuenta:(NSString*)numeroCuenta conClave:(int)clave ySaldo:(int)saldo {
	Usuario *nuevoUsuario = malloc(sizeof(Usuario));
	usuarioITF *interfazUsuario = [[usuarioITF alloc] init];
	nuevoUsuario->claveCodificada = [[NSMutableArray alloc] init];
	nuevoUsuario->nombre = nombre;
	nuevoUsuario->numeroCuenta = numeroCuenta;
	nuevoUsuario->clave = clave;
	nuevoUsuario->claveCodificada = [interfazUsuario crearClaveCodificada: clave];
	nuevoUsuario->saldo = saldo;
	[interfazUsuario mostrarClaveCodificada: nuevoUsuario];
	return nuevoUsuario;
}

-(NSMutableArray*)crearClaveCodificada:(int)clave {
	NSString *segmento = @"";
	NSMutableArray *claveCodificada = [[NSMutableArray alloc] init];
	int posicion1, posicion2, posicion3, posicion4, calculo;
	posicion4 = clave%10;
	posicion3 = (clave/10)%10;
	posicion2 = (clave/100)%10;
	posicion1 = (clave/1000)%10;
	
	calculo = posicion1*posicion2 + posicion3;
	do{
		if(calculo >= 65 && calculo <= 90) {
			segmento = [NSString stringWithFormat: @"%c", calculo];
			if(posicion1%2 != 0) {
				segmento = [NSString stringWithFormat: @"%@%@", segmento, @"b"];
			}
		}else if(calculo < 65)
			calculo = calculo + 2*posicion4 + posicion1 + posicion2 + posicion3;
		else
			calculo = calculo + posicion4 + posicion1 - 20;
	}while((calculo >= 65 && calculo <= 90) != 1);
	[claveCodificada addObject: [NSValue valueWithPointer: segmento]];
	
	calculo = 4*posicion3 + 2*posicion2 + 3*(posicion1 - posicion2);
	do{
		if(calculo >= 65 && calculo <= 90) {
			segmento = [NSString stringWithFormat: @"%c", calculo];
			if(posicion2%2 != 0) {
				segmento = [NSString stringWithFormat: @"%@%@", segmento, @"b"];
			}
		}else if(calculo < 65)
			calculo = calculo + 2*posicion4 + posicion1 + posicion2;
		else
			calculo = calculo + 2*posicion3 + posicion2 - 36;
	}while((calculo >= 65 && calculo <= 90) != 1);
	[claveCodificada addObject: [NSValue valueWithPointer: segmento]];
	
	calculo = 3*posicion4 + posicion1 + 2*posicion3 - 5;
	do{
		if(calculo >= 65 && calculo <= 90) {
			segmento = [NSString stringWithFormat: @"%c", calculo];
			if(posicion3%2 != 0) {
				segmento = [NSString stringWithFormat: @"%@%@", segmento, @"b"];
			}
		}else if(calculo < 65)
			calculo = calculo + 3*posicion4 + 2*posicion1;
		else
			calculo = calculo + posicion2 + 2*posicion1 - 30;
	}while((calculo >= 65 && calculo <= 90) != 1);
	[claveCodificada addObject: [NSValue valueWithPointer: segmento]];
	
	calculo = posicion4 + 2*posicion1 + 3*posicion3 - 5;
	do{
		if(calculo >= 65 && calculo <= 90) {
			segmento = [NSString stringWithFormat: @"%c", calculo];
			if(posicion4%2 != 0) {
				segmento = [NSString stringWithFormat:@"%@%@", segmento, @"b"];
			}
		}else if(calculo < 65)
			calculo = calculo + 3*posicion4 + 2*posicion1;
		else
			calculo = 2*calculo + posicion1 + posicion4 - 36;
	}while((calculo >= 65 && calculo <= 90) != 1);
	[claveCodificada addObject: [NSValue valueWithPointer: segmento]];
	
	return claveCodificada;
}

-(void)presentarUsuario:(Usuario*)usuario {
	NSLog(@"\tNombre: %@\tNumero de cuenta: %@\tSaldo: %d", usuario->nombre, usuario->numeroCuenta, usuario->saldo);
}

-(void)menuUsuario:(Usuario*)usuario {
	int op, valor;
	usuarioITF *interfazUsuario = [[usuarioITF alloc] init];
	do {
	NSLog(@"\t\tMENU PRINCIPAL");
	NSLog(@" [1] Consultar saldo actual\t\tDepositar [2]");
	NSLog(@" [3] Retirar\t\tSalir [4]");
	scanf("%d", &op);
		switch(op){
			case 1:
				NSLog(@"Su saldo actual es: %d", usuario->saldo);
			break;
			case 2:
				NSLog(@"Ingrese la cantidad que desea depositar:");
				scanf("%d", &valor);
					[interfazUsuario depositarSaldo: usuario depositara: valor];
			break;
			case 3:
				NSLog(@"Ingrese la cantidad que desea retirar:");
				scanf("%d", &valor);
					[interfazUsuario retirarSaldo: usuario retirara: valor];
			break;
			default:
			break;
		}
	}while(op<4);
}

-(void)depositarSaldo:(Usuario*)usuario depositara:(int)deposito {
	usuario->saldo = usuario->saldo + deposito;
	NSLog(@"Su saldo actual es %d", usuario->saldo);
}

-(void)retirarSaldo:(Usuario*)usuario retirara:(int)retiro {
	int temp = usuario->saldo;
	if(usuario->saldo < retiro) {
		NSLog(@"Esta tratando de retirar una cantidad mayor que su saldo actual");
		return;
	}else {
		temp = temp - retiro;
		usuario->saldo = temp;
		NSLog(@"Ha retirado $%d, su saldo actual es $%d", retiro, temp);
	}
}
                 
-(void)mostrarClaveCodificada:(Usuario*)usuario {
	NSLog(@"%@\t%@\t%@\t%@", *(NSString*)[[usuario->claveCodificada objectAtIndex: 0] pointerValue], *(NSString*)[usuario->claveCodificada objectAtIndex: 1],
	*(NSString*)[usuario->claveCodificada objectAtIndex: 2], *(NSString*)[usuario->claveCodificada objectAtIndex: 3]);
}          

@end